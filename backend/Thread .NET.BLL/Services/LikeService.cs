﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Hubs;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Like;
using Thread_.NET.Common.DTO.User;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public sealed class LikeService : BaseService
    {
        private readonly IHubContext<LikeHub> _likeHub;
        public LikeService(ThreadContext context, IMapper mapper, IHubContext<LikeHub> likeHub) : base(context, mapper)
        {
            _likeHub = likeHub;
        }

        public async Task<ICollection<ReactionDTO>> GetLikesByPostId(int id)
        {
            return await _context.PostReactions
                                    .Include(pr => pr.User)
                                        .ThenInclude(u => u.Avatar)
                                    .Where(x => x.PostId == id)
                                    .Select(x => _mapper.Map<ReactionDTO>(x))
                                    .ToListAsync();
        }

        public async Task LikePost(NewReactionDTO reaction)
        {
            var likesDelete = _context.PostReactions.Where(x => x.UserId == reaction.UserId && x.PostId == reaction.EntityId && x.IsLike == true).ToList();
            var likesUpdate = _context.PostReactions.Where(x => x.UserId == reaction.UserId && x.PostId == reaction.EntityId && x.IsLike == false).ToList();

            await ChangePostReaction(likesDelete, likesUpdate, reaction);
        }
        public async Task DisLikePost(NewReactionDTO reaction)
        {
            var dislikesDelete = _context.PostReactions.Where(x => x.UserId == reaction.UserId && x.PostId == reaction.EntityId && x.IsLike == false).ToList();
            var dislikesUpdate = _context.PostReactions.Where(x => x.UserId == reaction.UserId && x.PostId == reaction.EntityId && x.IsLike == true).ToList();

            await ChangePostReaction(dislikesDelete, dislikesUpdate, reaction);
        }

        private async Task ChangePostReaction(List<PostReaction> deleteReactions, List<PostReaction> updateReactions, NewReactionDTO newReaction)
        {
            Task notify = null;
            var authorId = _context.Posts.Where(post => post.Id == newReaction.EntityId).Select(post => post.AuthorId).FirstOrDefault();
            var userLiked = _context.Users.Where(user => user.Id == newReaction.UserId).FirstOrDefault();
            var author = _context.Users.Where(user => user.Id == authorId).FirstOrDefault();

            if (deleteReactions.Any())
            {
                _context.PostReactions.RemoveRange(deleteReactions);
            }
            else if (updateReactions.Any())
            {
                updateReactions.ForEach(x =>
                {
                    x.IsLike = newReaction.IsLike;
                    x.UpdatedAt = System.DateTime.Now;
                });
                _context.PostReactions.UpdateRange(updateReactions);

                if (newReaction.IsLike)
                {
                    notify = _likeHub.Clients.All.SendAsync("LikeNotify", new { user = _mapper.Map<UserDTO>(userLiked), authorId });
                }
            }
            else
            {
                _context.PostReactions.Add(new PostReaction
                {
                    PostId = newReaction.EntityId,
                    IsLike = newReaction.IsLike,
                    UserId = newReaction.UserId
                });

                if (newReaction.IsLike)
                {
                    notify = _likeHub.Clients.All.SendAsync("LikeNotify", new { user = _mapper.Map<UserDTO>(userLiked), authorId });
                }
            }
            await _context.SaveChangesAsync();

            await _likeHub.Clients.All.SendAsync("PostReactions", new { reactions = await GetLikesByPostId(newReaction.EntityId), postId = newReaction.EntityId });
            if (notify != null)
                await notify;

        }

        public async Task<ICollection<ReactionDTO>> GetLikesByCommentId(int id)
        {
            return await _context.CommentReactions
                                    .Include(pr => pr.User)
                                        .ThenInclude(u => u.Avatar)
                                    .Where(x => x.CommentId == id)
                                    .Select(x => _mapper.Map<ReactionDTO>(x))
                                    .ToListAsync();
        }

        public async Task LikeComment(NewReactionDTO reaction)
        {
            var likesDelete = _context.CommentReactions.Where(x => x.UserId == reaction.UserId && x.CommentId == reaction.EntityId && x.IsLike == true).ToList();
            var likesUpdate = _context.CommentReactions.Where(x => x.UserId == reaction.UserId && x.CommentId == reaction.EntityId && x.IsLike == false).ToList();

            await ChangeCommentReaction(likesDelete, likesUpdate, reaction);
        }

        public async Task DisLikeComment(NewReactionDTO reaction)
        {
            var dislikesDelete = _context.CommentReactions.Where(x => x.UserId == reaction.UserId && x.CommentId == reaction.EntityId && x.IsLike == false).ToList();
            var dislikesUpdate = _context.CommentReactions.Where(x => x.UserId == reaction.UserId && x.CommentId == reaction.EntityId && x.IsLike == true).ToList();

            await ChangeCommentReaction(dislikesDelete, dislikesUpdate, reaction);
        }

        private async Task ChangeCommentReaction(List<CommentReaction> deleteReactions, List<CommentReaction> updateReactions, NewReactionDTO newReaction)
        {
            if (deleteReactions.Any())
            {
                _context.CommentReactions.RemoveRange(deleteReactions);
            }
            else if (updateReactions.Any())
            {
                updateReactions.ForEach(x => {
                    x.IsLike = newReaction.IsLike;
                    x.UpdatedAt = System.DateTime.Now;
                });
                _context.CommentReactions.UpdateRange(updateReactions);
            }
            else
            {
                _context.CommentReactions.Add(new CommentReaction
                {
                    CommentId = newReaction.EntityId,
                    IsLike = newReaction.IsLike,
                    UserId = newReaction.UserId
                });
            }
            await _context.SaveChangesAsync();

            await _likeHub.Clients.All.SendAsync("CommentReactions", new { reactions = await GetLikesByCommentId(newReaction.EntityId), commentId = newReaction.EntityId });
        }
    }
}
