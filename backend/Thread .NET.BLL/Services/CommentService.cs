﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Exceptions;
using Thread_.NET.BLL.Hubs;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Comment;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public sealed class CommentService : BaseService
    {
        private readonly IHubContext<CommentHub> _commentHub;
        public CommentService(ThreadContext context, IMapper mapper, IHubContext<CommentHub> commentHub) : base(context, mapper) 
        {
            _commentHub = commentHub;
        }

        public async Task<CommentDTO> CreateComment(NewCommentDTO newComment)
        {
            var commentEntity = _mapper.Map<Comment>(newComment);

            _context.Comments.Add(commentEntity);
            await _context.SaveChangesAsync();

            var createdComment = await _context.Comments
                .Include(comment => comment.Author)
                    .ThenInclude(user => user.Avatar)
                .FirstAsync(comment => comment.Id == commentEntity.Id);

            var commentEntityDTO = _mapper.Map<CommentDTO>(createdComment);
            await _commentHub.Clients.All.SendAsync("NewComment", commentEntityDTO);

            return commentEntityDTO;
        }

        public async Task<CommentDTO> UpdateComment(UpdateCommentDTO commentDto)
        {
            var commentEntity = await _context.Comments
                .Include(comment => comment.Author)
                    .ThenInclude(user => user.Avatar)
                .Include(comment => comment.Reactions)
                    .ThenInclude(reaction => reaction.User)
                        .ThenInclude(user => user.Avatar)
                .FirstAsync(comment => comment.Id == commentDto.Id);

            if (commentEntity == null)
            {
                throw new NotFoundException(nameof(Comment), commentDto.Id);
            }

            commentEntity.Body = commentDto.Body;
            commentEntity.UpdatedAt = DateTime.Now;

            _context.Comments.Update(commentEntity);
            await _context.SaveChangesAsync();

            var commentEntityDTO = _mapper.Map<CommentDTO>(commentEntity);
            await _commentHub.Clients.All.SendAsync("UpdateComment", commentEntityDTO);

            return commentEntityDTO;
        }

        public async Task DeleteComment(int commentId)
        {
            var commentEntity = await _context.Comments.FirstOrDefaultAsync(u => u.Id == commentId);

            if (commentEntity == null)
            {
                throw new NotFoundException(nameof(Comment), commentId);
            }

            var commentReactions = _context.CommentReactions
                .Where(commentReaction => commentReaction.CommentId == commentId)
                .ToList();
            commentReactions.ForEach(commentReaction => _context.CommentReactions
                .Remove(commentReaction));

            _context.Comments.Remove(commentEntity);
            await _commentHub.Clients.All.SendAsync("DeleteComment", commentEntity.Id);

            await _context.SaveChangesAsync();
        }
    }
}
