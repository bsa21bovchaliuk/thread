import { Injectable } from '@angular/core';
import { AuthenticationService } from './auth.service';
import { Post } from '../models/post/post';
import { NewReaction } from '../models/reactions/newReaction';
import { PostService } from './post.service';
import { User } from '../models/user';
import { map, catchError } from 'rxjs/operators';
import { of, Subject } from 'rxjs';
import { CommentService } from './comment.service';
import { Comment } from '../models/comment/comment';


@Injectable({ providedIn: 'root' })
export class DisLikeService {
    public constructor(private authService: AuthenticationService, private postService: PostService, private commentService: CommentService) { }

    private unsubscribe$ = new Subject<void>();

    public dislikePost(post: Post, currentUser: User) {
        const innerPost = post;

        const reaction: NewReaction = {
            entityId: innerPost.id,
            isLike: false,
            userId: currentUser.id
        };

        // update current array instantly
        let hasReaction = innerPost.reactions.some((x) => x.user.id === currentUser.id);
        innerPost.reactions = hasReaction
            ? innerPost.reactions.filter((x) => x.user.id !== currentUser.id)
            : innerPost.reactions.concat({ isLike: false, user: currentUser });
        hasReaction = innerPost.reactions.some((x) => x.user.id === currentUser.id);

        return this.postService.dislikePost(reaction);
    }

    public dislikeComment(comment: Comment, currentUser: User) {
        const innerComment = comment;

        const reaction: NewReaction = {
            entityId: innerComment.id,
            isLike: false,
            userId: currentUser.id
        };

        // update current array instantly
        let hasReaction = innerComment.reactions.some((x) => x.user.id === currentUser.id);
        innerComment.reactions = hasReaction
            ? innerComment.reactions.filter((x) => x.user.id !== currentUser.id)
            : innerComment.reactions.concat({ isLike: false, user: currentUser });
        hasReaction = innerComment.reactions.some((x) => x.user.id === currentUser.id);

        return this.commentService.dislikeComment(reaction);
    }

}