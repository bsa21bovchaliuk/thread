import { Injectable } from '@angular/core';
import { HttpInternalService } from './http-internal.service';
import { NewComment } from '../models/comment/new-comment';
import { Comment } from '../models/comment/comment';
import { UpdateComment } from '../models/comment/update-comment';
import { Reaction } from '../models/reactions/reaction';
import { NewReaction } from '../models/reactions/newreaction';

@Injectable({ providedIn: 'root' })
export class CommentService {
    public routePrefix = '/api/comments';

    constructor(private httpService: HttpInternalService) { }

    public createComment(post: NewComment) {
        return this.httpService.postFullRequest<Comment>(`${this.routePrefix}`, post);
    }

    public updateComment(comment: UpdateComment) {
        return this.httpService.putFullRequest<Comment>(`${this.routePrefix}`, comment);
    }

    public deleteComment(commentID: number) {
        return this.httpService.deleteFullRequest(`${this.routePrefix}/${commentID}`);
    }

    public likeComment(reaction: NewReaction) {
        return this.httpService.postFullRequest(`${this.routePrefix}/like`, reaction);
    }

    public dislikeComment(reaction: NewReaction) {
        return this.httpService.postFullRequest(`${this.routePrefix}/dislike`, reaction);
    }
}
