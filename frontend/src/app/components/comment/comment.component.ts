import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { HubConnection } from '@microsoft/signalr';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';
import { UpdateComment } from 'src/app/models/comment/update-comment';
import { User } from 'src/app/models/user';
import { Subject, empty, Observable } from 'rxjs';
import { AuthDialogService } from 'src/app/services/auth-dialog.service';
import { AuthenticationService } from 'src/app/services/auth.service';
import { CommentService } from 'src/app/services/comment.service';
import { StatisticsDialogComponent } from '../statistics-dialog/statistics-dialog.component';
import { DisLikeService } from 'src/app/services/dislike.service';
import { LikeService } from 'src/app/services/like.service';
import { SnackBarService } from 'src/app/services/snack-bar.service';
import { Comment } from '../../models/comment/comment';
import { DialogType } from 'src/app/models/common/auth-dialog-type';

@Component({
    selector: 'app-comment',
    templateUrl: './comment.component.html',
    styleUrls: ['./comment.component.sass']
})
export class CommentComponent {
    @Input() public comment: Comment;
    @Input() public currentUser: User;
    @Input() public likeHub: HubConnection;

    @Output() deleteCommentEvent: EventEmitter<number> = new EventEmitter();
    @Output() updateCommentEvent: EventEmitter<Comment> = new EventEmitter();

    private unsubscribe$ = new Subject<void>();
    public updatedComment = {} as UpdateComment;

    public isEditing = false;

    public likesCount = 0;
    public dislikesCount = 0;
    public likeColor = 'primary';
    public dislikeColor = 'primary';

    public constructor(
        private authService: AuthenticationService,
        private authDialogService: AuthDialogService,
        private commentService: CommentService,
        private snackBarService: SnackBarService,
        private dialog: MatDialog,
        private likeService: LikeService,
        private dislikeService: DisLikeService,

    ) { }

    public ngOnInit() {
        this.updatedComment.id = this.comment.id;
        this.updatedComment.body = this.comment.body;
        this.registerHub();
        this.reactionsCalculate();
    }

    public registerHub() {
        this.likeHub.on('CommentReactions', (response) => {
            if (response) {
                this.updateCommentReactions(response);
            }
        });
    }

    public updateCommentReactions(response: any) {
        console.log("comment");
        if (response.commentId === this.comment.id) {
            this.reactionsCalculate();
        }
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public canEdit() {
        if (this.currentUser != null && this.comment.author.id == this.currentUser.id)
            return true;

        return false;
    }

    public editComment() {
        this.isEditing = true;
    }

    public close() {
        this.isEditing = false;
    }

    public save() {
        this.commentService
            .updateComment(this.updatedComment)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    if (resp) {
                        this.updateCommentEvent.emit(resp.body);
                    }
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
        this.isEditing = false;
    }

    public deleteComment() {
        this.commentService
            .deleteComment(this.comment.id)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    if (resp) {
                        this.deleteCommentEvent.emit(this.comment.id);
                    }
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }

    public reactionsCalculate() {
        this.likesCount = this.comment.reactions.filter(reaction => reaction.isLike == true).length;
        this.dislikesCount = this.comment.reactions.filter(reaction => reaction.isLike == false).length;

        this.likeColor = 'primary';
        this.dislikeColor = 'primary';

        if (this.currentUser != null) {
            var myReaction = this.comment.reactions.filter((x) => x.user.id === this.currentUser.id)[0];

            if (myReaction != null) {
                if (myReaction.isLike)
                    this.likeColor = 'accent';
                if (!myReaction.isLike)
                    this.dislikeColor = 'warn';
            }
        }
    }

    public showStatistics() {
        const dialog = this.dialog.open(StatisticsDialogComponent, {
            data: { reactions: this.comment.reactions },
            minWidth: 250,
            autoFocus: true,
            backdropClass: 'dialog-backdrop',
            position: {
                top: '0'
            }
        });
    }

    public likeComment() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.likeComment(this.comment, userResp)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe();

            return;
        }

        this.likeService
            .likeComment(this.comment, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe();
    }

    public dislikeComment() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.dislikeService.dislikeComment(this.comment, userResp)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe();

            return;
        }

        this.dislikeService
            .dislikeComment(this.comment, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe();
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.openAuthDialog();
                return empty();
            })
        );
    }
}
