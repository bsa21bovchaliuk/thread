import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { Reaction } from 'src/app/models/reactions/reaction';


@Component({
    selector: 'statistics-dialog',
    templateUrl: './statistics-dialog.component.html',
    styleUrls: ['./statistics-dialog.component.sass']
})
export class StatisticsDialogComponent implements OnInit, OnDestroy {
    public likeReactions = {} as Reaction[];
    public dislikeReactions = {} as Reaction[];

    public isLike = true;
    public likeColor = 'accent';
    public dislikeColor = 'primary';

    private unsubscribe$ = new Subject<void>();

    constructor(
        private dialogRef: MatDialogRef<StatisticsDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
    ) { }

    public ngOnInit() {

        this.likeReactions = this.data.reactions.filter(x => x.isLike);
        this.dislikeReactions = this.data.reactions.filter(x => !x.isLike);

        console.log(this.likeReactions);
        console.log(this.dislikeReactions);
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public close() {
        this.dialogRef.close(false);
    }

    public showLikeReactions() {
        this.isLike = true;
        this.likeColor = 'accent';
        this.dislikeColor = 'primary';
    }

    public showDislikeReactions() {
        this.isLike = false;
        this.likeColor = 'primary';
        this.dislikeColor = 'warn';
    }
}
